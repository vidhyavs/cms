import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../logo.png';

function Navigationbar() {
  return <div>

        <nav className="navbar navbar-expand-sm bg-danger navbar-light sticky-top">
            <a className="navbar-brand container" href="#">
                <img src={logo} width="70" height="70" alt="" loading="lazy" />
            </a>
            <div className="container-fluid justify-content-end">
                <ul className="navbar-nav">
                <li className="nav-item">
                    <Link className="nav-link text-white" to="/">Home</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link text-white" to="/favorite">Favorite</Link>
                </li>
               
                </ul>
            </div>
        </nav>
      </div>

}

export default Navigationbar;
