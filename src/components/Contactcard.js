import React from 'react';

function Contactcard({contact: {id, fav, Name, email, Phone, Address}, deleteItems, addFav}) {
  return  <div className='col-12 col-sm-8 col-md-4'>

        <div className='card shadow w-100 text-center mt-5'>
            <div className="card-header bg-danger text-white ">
                <div className='row'>
                    <div className='col-6'>{Name}</div>
                    <div onClick={()=>{addFav(id)}} className='col-2 offset-4'>
                        <i className={fav ? 'fas fa-star': 'far fa-star'}></i>
                    </div>
                </div>
            </div>
            <ul className="list-group list-group-flush">
                <li className="list-group-item">{email}</li>
                <li className="list-group-item">{Phone}</li>
                <li className="list-group-item">{Address}</li>
            </ul>  
            <div className='text-center'>
            <button type="button" className="btn btn-danger mt-3 mb-3" onClick={()=>{deleteItems(id)}}>Remove</button>
            </div>
           
        </div>
  </div>;
}

export default Contactcard;
