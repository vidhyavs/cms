import React from 'react';
import {useForm} from "react-hook-form";

function Form({formDataForm}) {
    const {                                                                     // destructure elements from react hook form
            register,
            handleSubmit, 
            formState:{ errors },
            reset,
        } = useForm();        

    const submitFunction = (data) =>{                                           // form validation and working
        data.id = Date.now();
        data.fav = false;
        formDataForm(data);
        reset();
    }

  return(
    <div className='container'>
        <div className='row justify-content-center pt-5 my-3'>
        <div className='col-12 col-sm-8 col-lg-6 shadow'>
        <h2 className='text-center text-danger mt-4 mb-2'>Contact Details</h2>    
        <form className='pt-3 row align-items-center flex-column' onSubmit={handleSubmit(submitFunction)}>
                <div className="form-outline mb-4 col-6">
                    <input type="text" className="form-control" placeholder='Name' {...register("Name", { required: 'Name is required'})} />
                    {errors.Name && (<small className='text-danger'>{errors.Name.message}</small>)}
                </div>
                <div className="form-outline mb-4 col-6">
                    <input type="email" className="form-control" placeholder='Email' {...register("email", { required: 'Email is required'})}/>
                    {errors.email && (<small className='text-danger'>{errors.email.message}</small>)}
                </div>
                <div className="form-outline mb-4 col-6">
                    <input type="number" className="form-control" placeholder='Phone' {...register("Phone", { required: 'Phone is required', pattern:{ value: /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[6789]\d{9}$/, message: "invalid phone number"}})}/>
                    {errors.Phone && (<small className='text-danger'>{errors.Phone.message}</small>)}
                </div>
                <div className="form-outline mb-4 col-6">
                    <textarea className="form-control" rows="4" placeholder='Address' {...register("Address", { required: 'Address is required'})}></textarea>
                    {errors.Address && (<small className='text-danger'>{errors.Address.message}</small>)}
                    
                </div>                
                <div className='text-center'>
                    <input type="submit" className="col-4 btn btn-danger btn-block mb-4" value="Add" />
                </div>
        </form>
        </div>
        </div>
    </div>
  )
}

export default Form;
