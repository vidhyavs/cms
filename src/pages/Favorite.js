import React from 'react';
import Contactcard from '../components/Contactcard';

function Favorite({contacts, deleteItems, addFav}) {
  return <div>
      <div className='container'>
      <div className='row justify-content-center'>
       
          { contacts.map((singleContact) =>{

              return( singleContact.fav &&(<Contactcard key={singleContact.id} contact={singleContact} deleteItems={deleteItems} addFav={addFav} />));
          })}

          {contacts.filter(singleContact=>singleContact.fav).length === 0 && (<div className="alert alert-danger shadow mt-5 text-center" role="alert"> No favourite contacts exists! </div>)}

      </div>
    </div>
  </div>;
}

export default Favorite;
