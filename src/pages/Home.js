import React from 'react';
import Contactcard from '../components/Contactcard';
import Form from '../components/Form';

function Home({formDataHome, contacts, deleteItems, addFav}) {

  return <div>
    <Form formDataForm={formDataHome}/>
    <div className='container'>
      <div className='row justify-content-center'>
       
          { contacts.map((singleContact) =>{
              return <Contactcard key={singleContact.id} contact={singleContact} deleteItems={deleteItems} addFav={addFav} />;
          })}
          {contacts.length === 0 && (
                <div className="alert alert-danger shadow mt-5 text-center" role="alert"> No contacts exists! </div>
          )}

      </div>
    </div>
    
  </div>;
}

export default Home;
