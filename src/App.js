import React, { useEffect, useState } from 'react';
import './App.css';

import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import Home from './pages/Home';
import Favorite from './pages/Favorite';
import Notfound from './pages/Notfound';
import Navigationbar from './components/Navigationbar';

function App() {
  const [contacts, setContacts] = useState([]);
  useEffect(() => {                                         //loads initialy when executes the app component
    const fetchContactsInitially = async() =>{
        const response = await fetchContacts();             
        setContacts(response);
    };
    fetchContactsInitially();                               // calls the function "fetchContacts()"
    
  }, []);
  

const FormDataApp = async(data) =>{                             //form submit

      const response = await fetch(" http://localhost:3004/contacts", {         // POST
        method:"POST",
        headers: {"Content-type": "application/json",},
        body: JSON.stringify(data),
      });
      const newdata = await response.json();
      if (response.ok){
        setContacts([...contacts, newdata]);
      }
  };

const fetchContacts = async()=>{                                              // fetch contents from db
      const response = await fetch(" http://localhost:3004/contacts");
      const response_data = await response.json();
      return response_data;
};

const deleteItems = async(id) =>{                               // delete item

    const response = await fetch(`http://localhost:3004/contacts/${id}`, {            // DELETE
        method:"DELETE",
      });

    if (response.status === 200){          // server confirmation
          let items = contacts.filter((singelData) =>{
            return singelData.id !== id;
        });
        setContacts(items);
    }
};
const getItemForUpdate = async(id) =>{
    const responseItem = await fetch(`http://localhost:3004/contacts/${id}`)
    const details = responseItem.json();
    return details;
};

const addFav= async(id) =>{                                                         // doubt to put
    const getupdateItem = await getItemForUpdate(id);
    const favValue = {...getupdateItem, fav: !getupdateItem.fav};

    const responseItem = await fetch(`http://localhost:3004/contacts/${id}`,{
      method: "PUT",
      headers:{"Content-type":"application/json",},
      body: JSON.stringify(favValue),
   
    });
    if (responseItem.status === 200){    
        let item = contacts.map((singelData) =>{
        return singelData.id === id ? {...singelData, fav: !singelData.fav} : singelData;  //spread operator is used to maintain the unupdated details and save fav
        });
        setContacts(item);
    }
};


  return (
    <div>
      <Router>  
        <Navigationbar/>
        
          <Routes>
            <Route exact path="/" element={<Home formDataHome={FormDataApp} contacts={contacts} deleteItems={deleteItems} addFav={addFav}/>}> </Route>
            <Route path="/Favorite" element={<Favorite contacts={contacts} deleteItems={deleteItems} addFav={addFav}/>}> </Route>
            <Route path="*" element={<Notfound />}></Route>
          </Routes>
      </Router>
    
    </div>  

  );
}

export default App;
